package com.rommelpalma.roomwordssample;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/*esta entrada representa una tabla*/
@Entity(tableName = "word_table")
public class Word {

    /*entidad principal o clave de la tabla*/
    @PrimaryKey
    /*indicamos que el parametro word nunca sera retornado como vacio*/
    @NonNull
    /*se especifica el nombre de la columna*/
    @ColumnInfo(name = "word")
    private String mWord;

    public Word(@NonNull String word) {this.mWord = word;}
    /*con el metodo getWord obtenemos el dato para que sea guardado en la base de datos*/
    public String getWord(){return this.mWord;}


}