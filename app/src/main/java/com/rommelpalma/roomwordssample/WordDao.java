package com.rommelpalma.roomwordssample;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;
/*Identifica q es una clase Dao para room*/
@Dao
/*denota que es una interfaz o una clase astracta   */
public interface WordDao {

    //inserta palabras repetidas allowing the insert of the same word multiple times by passing a
    // conflict resolution strategy
    @Insert(onConflict = OnConflictStrategy.IGNORE)

    /*se decalra un metodo para insertar una palabra*/
    void insert(Word word);

    /*se define una consulta sql en el @Query*/
    @Query("DELETE FROM word_table")

    /* se decalra metodo para eliminar palabras de la tabla*/
    void deleteAll();

    /* devuelve una lista de palabras ordenadas de forma ascendente */
    @Query("SELECT * FROM word_table ORDER BY word ASC")

    /* metodo para obtener las palabras del list word*/
    LiveData<List<Word>> getAlphabetizedWords();
}